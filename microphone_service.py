import logging

from redis import Redis

from config import settings
from utils.recognizer.audio import AudioSourceType
from utils.recognizer.recognizer import get_vosk_stream

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
if settings.DEBUG:
    LOGGER.setLevel(logging.DEBUG)


def main():
    redis_client = Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)
    stream = get_vosk_stream(
        vosk_host=settings.VOSK_SERVER_HOST,
        vosk_port=settings.VOSK_SERVER_PORT,
        stt_audio_encoding=settings.STT_AUDIO_ENCODING,
        sample_rate=settings.SAMPLE_RATE,
        chunk_size=settings.CHUNK_SIZE,
        channels=settings.CHANNELS,
        audio_source_type=AudioSourceType.MIC,
        input_device_name=settings.INPUT_DEVICE_NAME,
    )
    for sample_text in stream:
        LOGGER.debug(sample_text)
        if settings.ACTIVATION_WORD in sample_text:
            redis_client.publish(settings.WORKER_CHANNEL, sample_text)


if __name__ == "__main__":
    main()
