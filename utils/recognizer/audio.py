from enum import Enum
from contextlib import AbstractContextManager
from io import BytesIO
import subprocess
import typing as t

from config import settings

if settings.WITH_AUDIO:
    import pyaudio
else:

    class FakePyaudio:
        Stream = object()

    pyaudio = FakePyaudio()

from utils.recognizer import stt_service_pb2


class AudioSourceType(Enum):
    MIC = 1
    FILE = 2


class AudioException(Exception):
    def __str__(self):
        return f"AudioException: {self.msg}"


class CannotDecode(AudioException):
    msg = "Cannot decode data"


class AudioSource(AbstractContextManager):
    def __init__(
        self,
        stt_audio_encoding: str,
        sample_rate: int,
        chunk_size: int,
        channels: int,
        source_type: AudioSourceType,
        data: t.Optional[bytes] = None,
        file_audio_encoding: t.Optional[str] = None,
        input_device_name: t.Optional[str] = None,
    ):
        specification = stt_service_pb2.RecognitionSpec(
            partial_results=True,
            audio_encoding=stt_audio_encoding,
            sample_rate_hertz=sample_rate,
            enable_word_time_offsets=True,
            max_alternatives=5,
        )
        self.streaming_config = stt_service_pb2.RecognitionConfig(specification=specification)
        self.source_type = source_type
        self.audio_device = None
        self.chunk_size = chunk_size
        if source_type == AudioSourceType.MIC:
            self.source: pyaudio.Stream = self._prepare_mic_source(
                channels, sample_rate, chunk_size, input_device_name
            )
        elif source_type == AudioSourceType.FILE:
            self.source: BytesIO = self._prepare_file_source(
                channels, sample_rate, file_audio_encoding, data
            )

    def _prepare_mic_source(
        self,
        channels: int,
        sample_rate: int,
        chunk_size: int,
        input_device_name: t.Optional[str] = None,
    ) -> pyaudio.Stream:
        self.audio_device = pyaudio.PyAudio()
        open_kwargs = dict(
            format=pyaudio.paInt16,
            channels=channels,
            rate=sample_rate,
            frames_per_buffer=chunk_size,
            input=True,
        )
        if input_device_name:
            if input_device_index := self._get_device_index(input_device_name):
                open_kwargs["input_device_index"] = input_device_index
        audio = self.audio_device.open(**open_kwargs)
        return audio

    def _get_device_index(self, input_device_name: str) -> t.Optional[int]:
        info = self.audio_device.get_host_api_info_by_index(0)
        device_count = info.get("deviceCount")
        for i in range(0, device_count):
            if (
                self.audio_device.get_device_info_by_host_api_device_index(0, i).get(
                    "maxInputChannels"
                )
            ) > 0:
                device_info = self.audio_device.get_device_info_by_host_api_device_index(0, i)
                if input_device_name in device_info["name"]:
                    return device_info["index"]

    def _prepare_file_source(
        self, channels: int, sample_rate: int, audio_encoding: str, data: bytes
    ) -> BytesIO:
        assert data, "Data required for file source"
        assert audio_encoding, "Encoding required for file source"
        buffer = BytesIO(data)
        proc = subprocess.Popen(
            [
                "ffmpeg",
                "-i",
                "pipe:",
                "-ar",
                str(sample_rate),
                "-ac",
                str(channels),
                "-f",
                audio_encoding,
                "-",
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        wav_data, errs = proc.communicate(input=buffer.read())
        if errs:
            raise CannotDecode()
        return BytesIO(wav_data)

    def __enter__(self):
        return self

    def __exit__(self, *exc_details):
        if self.source_type == AudioSourceType.MIC:
            self.source.stop_stream()
            self.source.close()
            self.audio_device.terminate()

    def gen(self):
        yield stt_service_pb2.StreamingRecognitionRequest(config=self.streaming_config)
        data = self.source.read(self.chunk_size)
        while data != b"":
            yield stt_service_pb2.StreamingRecognitionRequest(audio_content=data)
            data = self.source.read(self.chunk_size)
