import logging
import typing as t
import asyncio

import grpc
from utils.recognizer import stt_service_pb2_grpc
from utils.recognizer.audio import AudioSource, AudioSourceType
from config import settings

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
if settings.DEBUG:
    LOGGER.setLevel(logging.DEBUG)


def get_vosk_stream(
    vosk_host: str,
    vosk_port: int,
    stt_audio_encoding: str,
    sample_rate: int,
    chunk_size: int,
    channels: int,
    audio_source_type: AudioSourceType,
    data: t.Optional[bytes] = None,
    file_audio_encoding: t.Optional[str] = None,
    input_device_name: t.Optional[str] = None,
) -> t.Generator[str, None, None]:
    channel = grpc.insecure_channel(f"{vosk_host}:{vosk_port}")
    stub = stt_service_pb2_grpc.SttServiceStub(channel)
    with AudioSource(
        stt_audio_encoding,
        sample_rate,
        chunk_size,
        channels,
        audio_source_type,
        data,
        file_audio_encoding,
        input_device_name,
    ) as source:
        streaming_service = stub.StreamingRecognize(source.gen())
        try:
            for resp in streaming_service:
                try:
                    sample = max(resp.chunks[0].alternatives, key=lambda item: item.confidence)
                    if sample.confidence:
                        yield sample.text
                except LookupError:
                    LOGGER.warning("No available chunks")
        except grpc._channel._Rendezvous as err:
            LOGGER.error("Error code %s, message: %s", err._state.code, err._state.details)


class GeneratorWrapper:
    def __init__(self, gen: t.Generator):
        self.gen = gen

    def __next__(self):
        try:
            return self.gen.__next__()
        except StopIteration:
            return


async def get_async_vosk_stream(
    vosk_host: str,
    vosk_port: int,
    stt_audio_encoding: str,
    sample_rate: int,
    chunk_size: int,
    channels: int,
    audio_source_type: AudioSourceType,
    data: t.Optional[bytes] = None,
    file_audio_encoding: t.Optional[str] = None,
) -> t.AsyncGenerator:
    stream = get_vosk_stream(
        vosk_host=vosk_host,
        vosk_port=vosk_port,
        stt_audio_encoding=stt_audio_encoding,
        sample_rate=sample_rate,
        chunk_size=chunk_size,
        channels=channels,
        audio_source_type=audio_source_type,
        data=data,
        file_audio_encoding=file_audio_encoding,
    )
    loop = asyncio.get_running_loop()
    stream = GeneratorWrapper(stream)
    result = await loop.run_in_executor(None, stream.__next__)
    while result is not None:
        yield result
        result = await loop.run_in_executor(None, stream.__next__)
