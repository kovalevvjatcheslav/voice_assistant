from aiohttp import ClientSession


async def get_city_location(
    openweather_api_url: str,
    openweather_api_key: str,
    city: str,
    session: ClientSession,
) -> dict:
    async with session.get(
        f"{openweather_api_url}/geo/1.0/direct?q={city}&appid={openweather_api_key}"
    ) as resp:
        if resp.status != 200:
            raise Exception(await resp.text())
        return (await resp.json())[0]


async def get_weather(
    openweather_api_url: str,
    openweather_api_key: str,
    lat: float,
    lon: float,
    session: ClientSession,
) -> dict:
    async with session.get(
        f"{openweather_api_url}/data/2.5/weather?lat={lat}&lon={lon}&units=metric&lang=ru"
        f"&appid={openweather_api_key}"
    ) as resp:
        if resp.status != 200:
            raise Exception(await resp.text())
        return await resp.json()


def build_weather_message(weather: dict) -> str:
    temp = weather["main"]["temp"]
    feels_like = weather["main"]["feels_like"]
    wind = weather["wind"]["speed"]
    weather_description = weather["weather"][0]["description"]
    return (
        f"Погода на улице такова: температура {temp} градусов, ощущается как {feels_like}, "
        f"ветер {wind} метров в секунду, снаружи {weather_description}"
    )


async def get_weather_msg(openweather_api_url: str, openweather_api_key: str, city: str) -> str:
    async with ClientSession() as session:
        geo = await get_city_location(openweather_api_url, openweather_api_key, city, session)
        weather = await get_weather(
            openweather_api_url, openweather_api_key, geo["lat"], geo["lon"], session
        )
        weather_msg = build_weather_message(weather)
    return weather_msg
