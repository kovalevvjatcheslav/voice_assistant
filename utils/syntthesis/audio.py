import asyncio
from contextlib import AbstractAsyncContextManager
from functools import partial
from io import BytesIO
import typing as t
import wave

from config import settings

if settings.WITH_AUDIO:
    import pyaudio

from aiohttp import ClientSession


class AudioPlaybackStream(AbstractAsyncContextManager):
    def __init__(self, buffer: BytesIO):
        self.buffer = buffer

    def __callback(
        self,
        in_data: t.Optional[bytes],
        frame_count: int,
        time_info: dict,
        status: int,
        wave_file: wave.Wave_read,
    ) -> tuple[bytes, int]:
        data = wave_file.readframes(frame_count)
        return data, pyaudio.paContinue

    async def __aenter__(self):
        self.wave_file = wave.open(self.buffer, "rb")
        audio_device = pyaudio.PyAudio()
        self.stream = audio_device.open(
            format=audio_device.get_format_from_width(self.wave_file.getsampwidth()),
            channels=self.wave_file.getnchannels(),
            rate=self.wave_file.getframerate(),
            output=True,
            stream_callback=partial(self.__callback, wave_file=self.wave_file),
        )
        self.stream.start_stream()
        while self.stream.is_active():
            await asyncio.sleep(0.1)

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.stream.stop_stream()
        self.stream.close()
        self.wave_file.close()


async def synthesis_audio(msg: str, tts_api_url: str, tts_voice: str) -> bytes:
    async with ClientSession() as session:
        async with session.get(
            f"{tts_api_url}?voice={tts_voice}"
            f"&lang=ru&vocoder=medium&denoiserStrength=0.005"
            f"&text={msg}"
            f"&speakerId="
            f"&ssml=false"
            f"&ssmlNumbers=true"
            f"&ssmlDates=true"
            f"&ssmlCurrency=true"
            f"&cache=false"
        ) as resp:
            if resp.status != 200:
                raise Exception(await resp.text())
            return await resp.read()
