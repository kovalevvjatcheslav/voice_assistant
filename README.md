### Название проекта: voice_assistant
### Участники: Вячеслав Ковалев kovalevvjatcheslav@gmail.com
### Описание проекта  
Голосовой ассистент способен распознавать команды и выполнять задачи (например сообщать погоду)  
Для доступа к api сервиса погоды необходимо в дирректории docker создать файл private.env с 
единственным значением  OPENWEATHER_API_KEY=<your-api-key>

Для сборки и запуска проекта необходимо:
 - установить docker
 - установить docker-compose
 - разрешить запуск docker без прав [суперпользователя](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
 - установить poetry ***pip install poetry=1.3.1***
 - собрать образы ***poetry run build-images***
 - поднять сервисы ***poetry run up***  

Веб-сервис будет доступен по адресу http://localhost:8080
