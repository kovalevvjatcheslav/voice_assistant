import typing as t
from pydantic import BaseSettings, Field, root_validator


class Settings(BaseSettings):
    VOSK_SERVER_HOST: str = Field(..., env="VOSK_SERVER_HOST")
    VOSK_SERVER_PORT: int = Field("5001", env="VOSK_SERVER_PORT")
    CHUNK_SIZE: int = Field("1024", env="CHUNK_SIZE")
    STT_AUDIO_ENCODING: str = Field("LINEAR16_PCM", env="STT_AUDIO_ENCODING")
    SAMPLE_RATE: int = Field(16000, env="SAMPLE_RATE")
    CHANNELS: int = Field(1, env="CHANNELS")
    ACTIVATION_WORD: str = Field(..., env="ACTIVATION_WORD")
    REDIS_HOST: str = Field("localhost", env="REDIS_HOST")
    REDIS_PORT: int = Field(6379, env="REDIS_PORT")
    REDIS_DB: str = Field("0", env="REDIS_DB")
    WORKER_CHANNEL: str = Field("worker", env="WORKER_CHANNEL")
    WEB_SERVICE_PORT: int = Field("8080", env="WEB_SERVICE_PORT")
    AUDIO_ENCODING: str = Field("s16le", env="AUDIO_ENCODING")

    tasks_map: list[dict] = Field(
        [{"key_word": "погода", "module": "tasks.weather"}], env="TASKS_MAP"
    )
    tasks_settings: dict[str, dict] = Field({"tasks.weather": {"default_city": "Chelyabinsk"}})
    OPENWEATHER_API_KEY: str = Field(..., env="OPENWEATHER_API_KEY")
    OPENWEATHER_API_URL: str = Field("http://api.openweathermap.org")
    TTS_HOST: str = Field(..., env="TTS_HOST")
    TTS_PORT: int = Field(5500, env="TTS_PORT")
    TTS_API_URI: t.Optional[str] = None
    TTS_VOICE: str = Field("larynx:nikolaev-glow_tts", env="TTS_VOICE")
    DEBUG: bool = Field(False, env="DEBUG")
    WITH_AUDIO: bool = Field(True, env="WITH_AUDIO")

    POSTGRES_HOST: str = Field("localhost", env="POSTGRES_HOST")
    POSTGRES_PORT: int = Field(5432, env="POSTGRES_PORT")
    POSTGRES_DB: str = Field("voice_assistant_db", env="POSTGRES_DB")
    POSTGRES_USER: str = Field("user", env="POSTGRES_USER")
    POSTGRES_PASSWORD: str = Field("very_strong_password", env="POSTGRES_PASSWORD")
    INPUT_DEVICE_NAME: t.Optional[str] = Field(None, env="INPUT_DEVICE_NAME")

    @root_validator
    def set_tts_api_uri(cls, values):
        values["TTS_API_URI"] = f"http://{values['TTS_HOST']}:{values['TTS_PORT']}/api/tts"
        return values


settings = Settings()
