import subprocess


def run_command(command: str):
    subprocess.run(command.split())


def build_images():
    commands = [
        ("docker build -f docker/kaldi_ru_grpc.docker -t kaldi_ru_grpc ."),
        "docker build --force-rm --no-cache --rm -f docker/web_app.docker -t web_app .",
    ]
    for command in commands:
        run_command(command)


def up():
    command = "docker-compose -f docker/docker_compose.yml up"
    run_command(command)


def down():
    command = "docker-compose -f docker/docker_compose.yml down"
    run_command(command)
