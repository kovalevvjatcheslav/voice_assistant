function init() {
  const constraints = {
    audio: true
  };

  window.ws = new WebSocket(`ws://${window.location.host}/voice-assistant`);
  window.ws.onmessage = on_receive_data;

  const audio_ctx = new AudioContext();
  let audio_url;
  let audio;
  const recorder_options = {
    mimeType: "audio/ogg"
  }

  navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
      window.media_recorder = new MediaRecorder(stream, recorder_options);
      window.media_recorder.ondataavailable = send_data;

    })
    .catch((error) => {
      console.log(error);
    });
}

function on_receive_data(msg) {
  toggle_waiting();
  if (msg.data.size > 0) {
    audioUrl = URL.createObjectURL(msg.data);
    audio = new Audio(audioUrl);
    audio.play();
  } else {
    set_not_found(true);
  }
}

function toggle_recording(e) {
  if (e.classList.contains("recording")) {
    // stop recording
    window.media_recorder.stop();
    e.classList.remove("recording");
  } else {
    // start recording
    if (!window.media_recorder)
        return;
    e.classList.add("recording");
    set_not_found(false);
    window.media_recorder.start();
  }
}

function toggle_waiting() {
  const waiting = document.getElementById("waiting");
  if (waiting.classList.contains("waiting")) {
    waiting.classList.remove("waiting");
  } else {
    waiting.classList.add("waiting");
  }
}

function set_not_found(visibility) {
  const not_found = document.getElementById("not_found");
  if (visibility) {
    not_found.classList.add("not_found");
  } else {
    not_found.classList.remove("not_found");
  }
}

function send_data(e) {
  window.ws.send(e.data);
  console.log("sending data...")
  toggle_waiting();
}

window.addEventListener('load', init);