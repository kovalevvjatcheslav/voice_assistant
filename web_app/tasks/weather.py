from utils.miscellaneous.weather import get_weather_msg
from utils.syntthesis.audio import synthesis_audio
from config import settings


async def main(*args) -> bytes:
    default_city = settings.tasks_settings["tasks.weather"]["default_city"]
    weather_msg = await get_weather_msg(
        settings.OPENWEATHER_API_URL,
        settings.OPENWEATHER_API_KEY,
        default_city,
    )
    return await synthesis_audio(weather_msg, settings.TTS_API_URI, settings.TTS_VOICE)
