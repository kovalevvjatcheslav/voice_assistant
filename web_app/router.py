from asyncio import exceptions
import logging
import importlib

from fastapi import APIRouter, Request, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse

from config import settings
from templates.template_processor import template_processor
from utils.recognizer.audio import AudioSourceType
from utils.recognizer.recognizer import get_async_vosk_stream
from sources import db


router = APIRouter()

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
if settings.DEBUG:
    LOGGER.setLevel(logging.DEBUG)

tasks_map = {}
for task in settings.tasks_map:
    tasks_map[task["key_word"]] = importlib.import_module(task["module"])


@router.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return template_processor.TemplateResponse(
        "index.html",
        {
            "request": request,
        },
    )


@router.websocket("/voice-assistant")
async def get_realtime(websocket: WebSocket):
    await websocket.accept()
    while True:
        try:
            data = await websocket.receive_bytes()

            stream = get_async_vosk_stream(
                vosk_host=settings.VOSK_SERVER_HOST,
                vosk_port=settings.VOSK_SERVER_PORT,
                stt_audio_encoding=settings.STT_AUDIO_ENCODING,
                sample_rate=settings.SAMPLE_RATE,
                chunk_size=settings.CHUNK_SIZE,
                channels=settings.CHANNELS,
                audio_source_type=AudioSourceType.FILE,
                data=data,
                file_audio_encoding=settings.AUDIO_ENCODING,
            )
            async for sample_text in stream:
                LOGGER.debug(sample_text)
                if settings.ACTIVATION_WORD in sample_text:
                    for command in sample_text.split():
                        if module := tasks_map.get(command):
                            response_data = await module.main()
                            await websocket.send_bytes(response_data)
                            await db.save_log(sample_text)
                else:
                    await websocket.send_bytes(b"")

        except (exceptions.CancelledError, KeyboardInterrupt, WebSocketDisconnect):
            await websocket.close()
            return
        except Exception as e:
            LOGGER.error(e)
            raise
