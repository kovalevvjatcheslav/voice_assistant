from io import BytesIO
import logging

from config import settings

from utils.syntthesis.audio import AudioPlaybackStream, synthesis_audio
from utils.miscellaneous.weather import get_weather_msg


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
if settings.DEBUG:
    LOGGER.setLevel(logging.DEBUG)


async def play_message(msg: str):
    data = await synthesis_audio(msg, settings.TTS_API_URI, settings.TTS_VOICE)
    buffer = BytesIO(data)
    async with AudioPlaybackStream(buffer):
        LOGGER.info("Audio playback...")


async def main(*args):
    default_city = settings.tasks_settings["tasks.weather"]["default_city"]
    weather_msg = await get_weather_msg(
        settings.OPENWEATHER_API_URL,
        settings.OPENWEATHER_API_KEY,
        default_city,
    )
    LOGGER.debug(weather_msg)
    await play_message(weather_msg)
