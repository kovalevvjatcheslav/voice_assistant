import importlib
import asyncio

from redis import asyncio as aioredis

from config import settings


async def main():
    redis_client = aioredis.Redis(
        host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB
    )
    tasks_map = {}
    for task in settings.tasks_map:
        tasks_map[task["key_word"]] = importlib.import_module(task["module"])

    async with redis_client.pubsub() as channel:
        await channel.subscribe(settings.WORKER_CHANNEL)
        while True:
            msg = await channel.get_message(ignore_subscribe_messages=True)
            if msg is not None and (data := msg.get("data")):
                data = data.decode()
                for command in data.split():
                    if module := tasks_map.get(command):
                        await module.main(data)
            await asyncio.sleep(0.01)


asyncio.run(main())
